<?php
    class MyCalculator{
        public $firstValue;
        public $secondValue;
        public $result;
        public function __construct($a,$b){
            $this->firstValue=$a;
            $this->secondValue=$b;
        }
        public function add(){
            $this->result=$this->firstValue+$this->secondValue;
            return $this->result;
        }
        public function sub(){
            $this->result=$this->firstValue-$this->secondValue;
            return $this->result;
        }
        public function multiply(){
            $this->result=$this->firstValue*$this->secondValue;
            return $this->result;
        }
        public function div(){
            $this->result=$this->firstValue/$this->secondValue;
            return $this->result;
        }

    }
$mycalc = new MyCalculator( 12, 6);

echo $mycalc-> add()."<br>";
echo $mycalc->sub()."<br>";
echo $mycalc-> multiply()."<br>";
echo $mycalc->div()."<br>";