<?php


    $startDate = new DateTime("1981-11-03");
    $endDate = new DateTime("2013-09-04");

    $result = $endDate->diff($startDate);
    $show = $result->format("%y years, %m months, %d days");
    echo $show;
